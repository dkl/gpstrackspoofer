"""
parsing gpx files to a list of gps points
"""

import gps
import xml.etree.ElementTree as ET


class GPXFile:
    def __init__(self, filename):
        self.filename = filename
        self.tree = None
        self.xmlns = {"":"http://www.topografix.com/GPX/1/1"}

    def _lazy_parse(self):
        if not self.tree:
            self.tree = ET.parse(self.filename)

    def _get_namespace(self):
        self._lazy_parse()

    def get_points(self, reverse=False):
        tree = ET.parse(self.filename)
        root = tree.getroot()

        track_points = root.find('trk', self.xmlns).find('trkseg', self.xmlns).findall('trkpt', self.xmlns)
        if reverse:
            track_points = list(track_points)[::-1]

        for point in track_points:
            lat = float(point.attrib['lat'])
            lon = float(point.attrib['lon'])
            yield gps.GPSPoint(lat, lon)


