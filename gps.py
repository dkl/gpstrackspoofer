"""
Calculate the distance between two gps points and
calculating steps of a certain lengs between two points to create a route
on a direct line between those points
"""

import math
import random

EARTH_RADIUS = 6371000


class GPSPoint():
    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon

    @classmethod
    def clone(cls, other_point):
        return GPSPoint(other_point.lat, other_point.lon)

    @staticmethod
    def _radians(a, b):
        distance = 2 * EARTH_RADIUS * math.pi * (a-b)/360
        return distance

    def _arc(self, distance):
        arc = distance * 360/(2 * EARTH_RADIUS * math.pi)
        return arc

    def distance(self, other_point):
        distance_x = self._radians(self.lat, other_point.lat)
        distance_y = self._radians(self.lon, other_point.lon)
        distance = math.sqrt(math.pow(distance_x, 2) + math.pow(distance_y, 2))
        return distance

    def __add__(self, other):
        new_x = self.lat + other.lat
        new_y = self.lon + other.lon
        return GPSPoint(new_x, new_y)

    def __sub__(self, other):
        new_x = self.lat - other.lat
        new_y = self.lon - other.lon
        return GPSPoint(new_x, new_y)

    def __str__(self):
        return f"Point {self.lat}, {self.lon}"

    def __eq__(self, other):
        if not other:
            return not self.lat or not self.lon
        elif isinstance(other, GPSPoint):
            return self.lat == other.lat and self.lon == other.lon
        else:
            return False

    def step_north(self, step_distance):
        arc = self._arc(step_distance)
        new_point = GPSPoint(self.lat + arc, self.lon)
        return new_point

    def step_east(self, step_distance):
        arc = self._arc(step_distance)
        new_point = GPSPoint(self.lat, self.lon + arc)
        return new_point

    def step_towards(self, other_point, step_distance):
        d = self.distance(other_point)
        step_fraction = step_distance/d
        step_x = (other_point.lat - self.lat) * step_fraction
        step_y = (other_point.lon - self.lon) * step_fraction
        new_point = GPSPoint(self.lat + step_x, self.lon + step_y)
        return new_point

    def route(self, other_point, step_distance):
        d = self.distance(other_point)
        next_point = GPSPoint.clone(self)
        while d > step_distance:
            d -= step_distance
            next_point = next_point.step_towards(other_point, step_distance)
            yield next_point
        else:
            yield other_point

    def jitter(self):
        lat_jitter_step = self._arc(random.gauss(0, 0.5))
        lon_jitter_step = self._arc(random.gauss(0, 0.5))
        return GPSPoint(self.lat + lat_jitter_step, self.lon + lon_jitter_step)


class Track():
    def __init__(self, *args, points=None):
        if points:
            self.it = iter(points)
        else:
            self.it = iter(args)

    def route(self, step_distance):
        try:
            last_point = next(self.it)
        except StopIteration:
            return

        for point in self.it:
            for p in last_point.route(point, step_distance):
                yield p
            last_point = point




