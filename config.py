import json

CONFIG_FILE = "./config.json"
CACHE_FILE = ""
GPX_FILE = ""
SPOOFER_BIN = ""

try:
    with open(CONFIG_FILE, 'r') as cache_file:
        config = json.loads(cache_file.read())

    for key in config:
        if key == "CACHE_FILE":
            CACHE_FILE = config[key]
        elif key == "GPX_FILE":
            GPX_FILE = config[key]
        elif key == "SPOOFER_BIN":
            SPOOFER_BIN = config[key]
        else:
            print("unknown config key")
except IOError:
    print(f"Could not load config from {CONFIG_FILE}")

