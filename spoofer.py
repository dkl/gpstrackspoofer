import subprocess
import threading
import time
import cacher
import config
import random
import math

class SpooferError(Exception):
    pass


class Spoofer:
    def __init__(self, start_point):
        self.gps_point = start_point
        self.stop_thread = False
        self.thread = threading.Thread(target=self._send_new_location, args=(), daemon=True)
        self.thread.start()
        print("Thread started")

    def set_location(self, gps_point):
        self.gps_point = gps_point

    @staticmethod
    def _call_spoofer_bin(gps_point):
        """ This assumes that the bin takes a lat and lon as arguments:
            bin <lat> <lon>
        """
        cmd = f"{config.SPOOFER_BIN} {str(gps_point.lat)} {str(gps_point.lon)}"
        proc = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        if proc.returncode == 0:
            return proc.stdout.decode()
        if proc.returncode == 255 and proc.stdout == b'ERROR: No device found!\n':
            # This can occasionally happen and for lack of better solution just don't raise an
            # exception in that case
            raise SpooferError(proc.stdout.decode())
        elif proc.returncode == -11:
            # device unplugged
            raise SpooferError(proc.stdout.decode())
        else:
            raise SpooferError(proc.stdout.decode())

    def _send_new_location(self):
        error_count = 0
        while not self.stop_thread:
            time.sleep(math.fabs(random.gauss(1, 0.5)))

            if self.gps_point == None:
                continue

            if error_count > 5:
                raise SpooferError("Can't communicate with device")
            try:
                output = Spoofer._call_spoofer_bin(self.gps_point.jitter())
            except SpooferError as e:
                error_count += 1
                print(str(e))
                continue

            error_count = 0
            print(f"set position to {str(self.gps_point.lat)},{str(self.gps_point.lon)}")
            cacher.set_position(self.gps_point)
            cacher.write_cache()
