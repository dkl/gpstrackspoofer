import json
import gps
import os
import config

class CacheError(Exception):
    pass


class Cache:
    def __init__(self):
        self.position = gps.GPSPoint(None, None)

    def to_json(self):
        obj = {
            "position": {
                "lat": self.position.lat,
                "lon": self.position.lon,
            }
        }
        return json.dumps(obj)

    @staticmethod
    def none_cache():
        return {
            "position": {
                "lat": None,
                "lon": None,
            }
        }

    @staticmethod
    def validate_json(json_obj):
        if "position" not in json_obj:
            raise CacheError()
        if "lat" not in json_obj["position"]:
            raise CacheError()
        if "lat" not in json_obj["position"]:
            raise CacheError()

    def from_json(self, raw_json):
        try:
            json_obj = json.loads(raw_json)
            Cache.validate_json(json_obj)
        except (json.decoder.JSONDecodeError, CacheError):
            return Cache.none_cache()

        self.position = gps.GPSPoint(
            lat=json_obj["position"]["lat"],
            lon=json_obj["position"]["lon"]
        )



CACHE = Cache()  # poor man's singleton


def set_position(gps_point):
    CACHE.position = gps_point


def get_position():
    return CACHE.position


def write_cache():
    with open(config.CACHE_FILE, 'w+') as cache_file:
        cache_file.write(CACHE.to_json())

def load_cache():
    if not os.path.exists(config.CACHE_FILE):
        write_cache()

    try:
        with open(config.CACHE_FILE, 'r') as cache_file:
            CACHE.from_json(cache_file.read())

    except IOError:
        print("Can't load cache")
    return CACHE
