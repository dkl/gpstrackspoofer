#!/usr/bin/env python
import time
import sys
import signal
import atexit
import argparse
import config
import gps
import gpx
import wasd
import spoofer
import cacher


def args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--interactive", help="interactive mode to navigate with WASD", action='store_true')
    parser.add_argument("--starting", help="starting point as lon,lat", type=str, default=None)
    return parser.parse_args()


def walk_gpx_track(start_point):
    gpx_file = gpx.GPXFile(config.GPX_FILE)
    points = gpx_file.get_points(reverse=False)
    track = gps.Track(points=points)
    spoofer_obj = spoofer.Spoofer(start_point)
    kmph = 12
    mps = kmph/3.6
    step = 1
    wait = step/mps
    start = False
    for p in track.route(step):
        if start_point == None or p.lat == start_point.lat and p.lon == start_point.lon:
            start = True
        if start:
            spoofer_obj.set_location(p)
            time.sleep(wait)


def on_start():
    print("started")
    cacher.load_cache()
    print(f"loaded position from cache:\n {cacher.CACHE.position}\n")


def on_exit(signum=None, frame=None):
    cacher.write_cache()
    print("App terminates and caches position.")
    if signum is not None and frame is not None:
        sys.exit(0)


if __name__ == '__main__':
    on_start()
    signal.signal(signal.SIGINT, on_exit)
    atexit.register(on_exit)

    args = args()
    start_point = cacher.get_position()
    if "starting" in args and args.starting and ',' in args.starting:
        lon, lat = args.starting.split(",")
        start_point = gps.GPSPoint(float(lon), float(lat))
        print(f"cache ignored in favor of manual starting position: \n {start_point}\n")

    if args.interactive:
        if not start_point:
            print("Needs a starting point")
            sys.exit(1)
        spoofer_obj = spoofer.Spoofer(start_point)
        step = 0.5
        wasd.Controller(start_point, step, spoofer_obj)
    else:
        walk_gpx_track(start_point)
