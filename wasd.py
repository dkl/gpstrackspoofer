import threading
from pynput import keyboard


class Controller():
    def __init__(self, start_point, step, spoofer):
        self.current_point = start_point
        self.step = step
        self.spoofer = spoofer
        listener = keyboard.Listener(on_press=self.on_press, surpress=True)
        listener.start()
        listener.join()

    def on_press(self, key):
        if key == keyboard.Key.esc:
            return False
        elif key == keyboard.Key.up:
            self.current_point = self.current_point.step_north(self.step)
        elif key == keyboard.Key.right:
            self.current_point = self.current_point.step_east(self.step)
        elif key == keyboard.Key.down:
            self.current_point = self.current_point.step_north(0-self.step)
        elif key == keyboard.Key.left:
            self.current_point = self.current_point.step_east(0-self.step)

        print(key)
        self.spoofer.set_location(self.current_point)
        return True
